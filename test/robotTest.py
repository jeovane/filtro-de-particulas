import random
import unittest

import robot
import robotOriginal

N = 1000
T = 100

class robotTest(unittest.TestCase):

    def test_move(self):
        myRobot = robotOriginal.robot()
        myRobot.set(0, 0, 0)
        myRobot = myRobot.move(0, 5)
        self.assertEqual(myRobot.x, 5)
        self.assertEqual(myRobot.y, 0)

    def test_convertion_original(self):
        p = []
        myrobot = robotOriginal.robot()
        for i in range(N):
            x = robotOriginal.robot()
            x.set_noise(0.01, 0.01, 0.025)
            p.append(x)

        evalOriginal = robotOriginal.eval(myrobot, p)
        for t in range(T):

            myrobot = myrobot.move(0.1, 5.0)
            Z = myrobot.sense()

            p2 = []
            for i in range(N):
                p2.append(p[i].move(0.1, 5.0))
            p = p2

            # calcula o erro
            w = []
            for i in range(N):
                w.append(p[i].measurement_prob(Z))

            p3 = []
            index = int(random.random() * N)
            beta = 0.0
            mw = max(w)
            for i in range(N):
                beta += random.random() * 2.0 * mw
                while beta > w[index]:
                    beta = beta - w[index]
                    index = (index + 1) % N
                p3.append(p[index])
            p = p3
        evalFinal = robotOriginal.eval(myrobot, p)
        divEval = evalFinal / evalOriginal
        self.assertLessEqual(divEval, 0.1)
        # for i in range(N):
        #     print p[i]


    def test_convertion(self):
        p = []
        myrobot = robot.robot()

        # cria as particulas
        for i in range(N):
            x = robot.robot()
            x.set_noise(0.05, 0.05, 5.0)
            p.append(x)
        evalOriginal = robotOriginal.eval(myrobot, p)
        for t in range(T):
            # move o robo e mede a distancia entre ele e os marcadores
            myrobot = myrobot.move(0.1, 5.0)
            Z = myrobot.sense()

            # move as particulas
            p = myrobot.updateParticleSet(0.1, 5.0, p)

            # calcula o erro
            w = []
            w = myrobot.importantWeight(Z, p)

            # redesenha as particulas
            p = myrobot.resampling(p, w)

        evalFinal = robotOriginal.eval(myrobot, p)
        divEval = evalFinal/evalOriginal
        self.assertLessEqual(divEval, 0.1)
        # for i in range(N):
        #     print p[i]

        # def set_test(self):
        #
        # def set_noise_test(self):

        # def sense_test(self):
        #
        #
        # def Gaussian_test(self):
        #
        # def measurement_prob_test(self):
        #
        # def eval_test(self):
        #
        # def updateParticleSet_test(self):
        #
        # def importantWeight_test(self):
        #
        # def normalizeWeight_test(self):
        #
        # def resampling_test(self):

unittest.main()