from math import *
import random
import math
from intersection import intersection
import util
import time

# landmarks = [[-2.5000, -0.2500], [2.5000, -0.1750], [-0.2000, -2.5000], [-0.2500, 2.5000]]
# world_size = 2.5000
# landmarks = [[2.5, 0], [5.0, 2.5], [2.5, 5.0], [0, 2.5]]
# landmarks = [[2.5, 0], [5.0, 2.5]]
# world_size = 5
# landmarks = [[20.0, 20.0], [80.0, 80.0], [20.0, 80.0], [80.0, 20.0]]
world_size = 100
particles = []
wheelR = 0.195 / 2.0
intersectionInstance = intersection.intersection()


class robot:
    def __init__(self):
        self.x = random.random() * world_size
        self.y = random.random() * world_size
        self.orientation = random.random() * 2.0 * pi
        self.forward_noise = 0.0
        self.turn_noise = 0.0
        self.sense_noise = 0.0

    def set(self, new_x, new_y, new_orientation):
        if new_x < 0 or new_x >= world_size:
            raise ValueError, 'X coordinate out of bound'
        if new_y < 0 or new_y >= world_size:
            raise ValueError, 'Y coordinate out of bound'
        if new_orientation < 0 or new_orientation >= 2 * pi:
            raise ValueError, 'Orientation must be in [0..2pi]'
        self.x = float(new_x)
        self.y = float(new_y)
        self.orientation = float(new_orientation)

    def set_noise(self, new_f_noise, new_t_noise, new_s_noise):
        # makes it possible to change the noise parameters
        # this is often useful in particle filters
        self.forward_noise = float(new_f_noise)
        self.turn_noise = float(new_t_noise)
        self.sense_noise = float(new_s_noise)

    def sense(self, landmarks, px, py):
        Z = {}
        for i in landmarks:
            dist = math.sqrt((px - landmarks[i].x) ** 2 + (py - landmarks[i].y) ** 2)
            dist += random.gauss(0.0, self.sense_noise)
            Z[i] = dist
        return Z

    def sense2(self, clientID, landmarks, px, py, angulo, angulosDeDeteccao):
        Z = {}
        landmarkHendles = []
        for i in landmarks:
            landmarkHendles.append(landmarks[i].handle)

        segments = util.util().getSegments(clientID, landmarkHendles)
        angulo %= 2 * math.pi
        self.set(px, py, angulo)
        self.set_noise(0.05, 0.05, 5.0)
        Z = intersection.intersection().getDetectedIntersection(clientID, self, angulosDeDeteccao, segments)
        return Z

    def move(self, turn, forward):
        if forward < 0:
            raise ValueError, 'Robot cant move backwards'

        # turn, and add randomness to the turning command
        orientation = self.orientation + float(turn) + random.gauss(0.0, self.turn_noise)
        if orientation < 0:
            orientation = 2*math.pi + orientation
        orientation %= 2 * math.pi

        # move, and add randomness to the motion command
        dist = float(forward) + random.gauss(0.0, self.forward_noise)
        x = self.x + (math.cos(orientation) * dist)
        y = self.y + (math.sin(orientation) * dist)
        x %= world_size  # cyclic truncate C
        y %= world_size

        # set particle
        res = robot()
        res.set(x, y, orientation)
        res.set_noise(self.forward_noise, self.turn_noise, self.sense_noise)
        return res

    # Calcula a densidade da probabilidade da distribuicao normal de x
    # mu (media) eh a distancia entre a particula e o landmarks[i]
    # sigma (desvio padrao) eh o ruido do sensor
    # x eh a distancia entre o robo e o landmarks[i]
    def Gaussian(self, mu, sigma, x):
        return exp(- ((mu - x) ** 2) / (sigma ** 2) / 2.0) / sqrt(2.0 * pi * (sigma ** 2))

    def measurement_prob(self, landmarks, Z):
        # calculates how likely a measurement should be
        prob = 1.0
        for i in landmarks:
            # dist = math.sqrt((self.x - landmarks[i].x) ** 2 + (self.y - landmarks[i].y) ** 2)
            # prob *= self.Gaussian(dist, self.sense_noise, Z[i])
            stepProb = self.Gaussian(landmarks[i], self.sense_noise, Z[i])
            prob *= stepProb
        return prob

    def __repr__(self):
        return '[x=%.6s y=%.6s orient=%.6s]' % (str(self.x), str(self.y), str(self.orientation))

    def eval(r, p):
        sum = 0.0
        for i in range(len(p)):  # calculate mean error
            dx = (p[i].x - r.x + (world_size / 2.0)) % world_size - (world_size / 2.0)
            dy = (p[i].y - r.y + (world_size / 2.0)) % world_size - (world_size / 2.0)
            err = math.sqrt(dx * dx + dy * dy)
            sum += err
        return sum / float(len(p))

    def updateParticleSet(self, teta, r, particles, afterTeta):
        particlesUpdated = []

        for i in range(len(particles)):
            particles[i].orientation = afterTeta
            particlesUpdated.append(particles[i].move(teta, r))

        return particlesUpdated

    def importantWeight(self, clientId, particles, anguloDeDeteccao, segments, Z):
        w = []

        for i in range(len(particles)):
            p = particles[i]
            obstaculosDetectadosPorParticula = intersectionInstance.getDetectedIntersection(clientId, p, anguloDeDeteccao, segments)
            w.append(particles[i].measurement_prob(obstaculosDetectadosPorParticula, Z))
        return w

    def normalizeWeight(self, w):
        wNormalized = []
        totalSum = sum(w)
        for i in range(len(w)):
            wNormalized.append(w[i] / totalSum)
        return wNormalized

    def resampling(self, p, w, r):
        coordx = random.sample(range(0, 1000), 1000)
        coordy = random.sample(range(0, 1000), 1000)
        newp = []
        mw = max(w)
        beta = 0.0
        index = int(random.random() * len(p))
        for i in range(len(p)):
            beta += random.random() * 2.0 * mw
            while beta > w[index]:
                beta = beta - w[index]
                index = (index + 1) % len(p)

            newp.append(p[index])
        # result = robot.eval(r, newp)
        # if result >= 0 and result <= 10:
        #     time.sleep(600)
        multiple = int(len(p) / 100)
        for i in range(len(p)):
            if i % multiple == 0:
                random.shuffle(coordx)
                random.shuffle(coordy)
                random.shuffle(newp)
                ran3 = random.random() * 2.0 * pi
                x = coordx[i]
                y = coordy[i]
                x %= 100
                y %= 100
                r = robot()
                r.set(x, y, ran3)
                r.set_noise(0.05, 0.05, 5.0)
                newp[i] = r


                print(i)
            if len(p) != 1000:
                exit(-1)

        return newp

    def resampling2(self, p, w):
        newp = []
        mw = max(w)
        beta = 0.0
        index = int(random.random() * len(p))
        for i in range(len(p)):
            beta += random.random() * 2.0 * mw
            while beta > w[index]:
                beta = beta - w[index]
                index = (index + 1) % len(p)

            newp.append(p[index])
        return newp