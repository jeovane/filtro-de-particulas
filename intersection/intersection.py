import math

import ponto
import vector
import particle
import segment
import vrep
import random


inputInts = [1, 2, 3]
inputStrings = ['Hello', 'world!']
inputBuffer = bytearray()
inputBuffer.append(78)
inputBuffer.append(42)
inf = 110
class intersection:

    def dist(self, p1, p2):
        d = math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2)
        return d

    def translate(self, p, v):
        xtranslate = p.x + v.x
        ytranslate = p.y + v.y
        return ponto.ponto(xtranslate, ytranslate)

    def toVec(self, pontoA, pontoB):
        xVec = pontoB.x - pontoA.x
        yVec = pontoB.y - pontoA.y
        return vector.vector(xVec, yVec)

    def scale(self, vec, scaleValue):
        xScale = vec.x * scaleValue
        yScale = vec.y * scaleValue
        return vector.vector(xScale, yScale)

    def dot(self, vectorA, vectorB):
        return (vectorA.x * vectorB.x) + (vectorA.y * vectorB.y)

    def normSq(self, vec):
        return (vec.x * vec.x) + (vec.y * vec.y)

    def onSegment(self, p, q, r):
        if q.x <= max(p.x, r.x) and q.x >= min(p.x, r.x) and q.y <= max(p.y, r.y) and q.y >= min(p.y, r.y):
            return True
        else:
            return False

    def orientation(self, p, q, r):
        val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y)
        if val == 0:
            return 0
        elif val > 0:
            return 1
        else:
            return 2

    def doIntersect(self, segment1, segment2):
        p1 = segment1.pontoA
        q1 = segment1.pontoB
        p2 = segment2.pontoA
        q2 = segment2.pontoB
        o1 = self.orientation(p1, q1, p2)
        o2 = self.orientation(p1, q1, q2)
        o3 = self.orientation(p2, q2, p1)
        o4 = self.orientation(p2, q2, q1)
        if o1 != o2 and o3 != o4:
            return True
        if not o1 and self.onSegment(p1, p2, q1):
            return True
        if not o2 and self.onSegment(p1, q2, p1):
            return True
        if not o3 and self.onSegment(p2, p1, q2):
            return True
        if not o4 and self.onSegment(p2, q1, q2):
            return True
        return False

    def distToLine(self, p, a, b):
        vectorAP = self.toVec(a, p)
        vectorAB = self.toVec(a, b)
        u = self.dot(vectorAP, vectorAB) / self.normSq(vectorAB)
        c = self.translate(a, self.scale(vectorAB, u))
        return self.dist(p, c)

    def distToSegment(self, p, a, b):
        vectorAP = self.toVec(a, p)
        vectorAB = self.toVec(a, b)
        u = self.dot(vectorAP, vectorAB) / self.normSq(vectorAB)

        if u < 0.0:
            # c = ponto.ponto(a.x, a.y)
            return self.dist(p, a)
        elif u > 1.0:
            # ponto.ponto(b.x, b.y)
            return self.dist(p, b)
        else:
            return self.distToLine(p, a, b)

    def closestIntersection(self, particle, segment, segmentList):
        distance = inf
        # m = ponto.ponto(-inf * 1.0, -inf * 1.0)
        # pontoAux = ponto.ponto(0, 0)
        for i in range(len(segmentList)):
            res = self.doIntersect(segment, segmentList[i])
            if res:
                # pmx = (segmentList[i].pontoA.x + segmentList[i].pontoB.x)/2
                # pmy = (segmentList[i].pontoA.y + segmentList[i].pontoB.y)/2
                # d = math.sqrt((particle.position.x - pmx) ** 2 + (particle.position.y - pmy) ** 2)
                d = self.distToSegment(particle.position, segmentList[i].pontoA, segmentList[i].pontoB)
                if d < distance:
                    distance = d
                    # m = pontoAux
        return distance

    def degToRad(self, angle):
        return angle * math.pi / 180.0

    def getDetectedIntersection(self, clientId, p, angleArray, segments):
        intersectionDetected = {}
        pontoA = ponto.ponto(p.x, p.y)
        for i in angleArray:
            # angle = angleArray[i]
            angle = angleArray[i] + p.orientation
            # if angle < 0:
            #     angle = 2 * math.pi + angle
            angle %= 2 * math.pi
            newParticle = particle.particle(pontoA, angle)
            pontoBP = ponto.ponto(p.x + 2 * math.cos(p.orientation), p.y + 2 * math.sin(p.orientation))
            pontoB = ponto.ponto(p.x + inf * math.cos(angle), p.y + inf * math.sin(angle))
            fakeSegment = segment.segment(pontoA, pontoB)
            inputFloats = [(pontoA.x/20) - 2.5, pontoA.y/20 - 2.5, pontoB.x/20 -2.5, pontoB.y/20 -2.5]
            inputFloats2 = [(pontoA.x/20) - 2.5, pontoA.y/20 - 2.5, pontoBP.x/20 -2.5, pontoBP.y/20 -2.5]
            # vrep.simxCallScriptFunction(clientId, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
            #                             "desenharLinha2", inputInts, inputFloats2, inputStrings,
            #                             inputBuffer, vrep.simx_opmode_oneshot_wait)
            # vrep.simxCallScriptFunction(clientId, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
            #                             "desenharLinha2", inputInts, inputFloats, inputStrings,
            #                             inputBuffer, vrep.simx_opmode_oneshot_wait)
            distance = self.closestIntersection(newParticle, fakeSegment, segments)
            intersectionDetected[i] = distance + random.gauss(0.0, p.sense_noise)
        return intersectionDetected