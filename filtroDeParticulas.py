# Make a robot called myrobot that starts at
# coordinates 30, 50 heading north (pi/2).
# Have your robot turn clockwise by pi/2, move
# 15 m, and sense. Then have it turn clockwise
# by pi/2 again, move 10 m, and sense again.
#
# Your program should print out the result of
# your two sense measurements.
#
# Don't modify the code below. Please enter
# your code at the bottom.

# Robot spec: http://www.mobilerobots.com/Libraries/Downloads/Pioneer3DX-P3DX-RevA.sflb.ashx
import landmark
import vrep
import math
import robot
import util
import time
from intersection import ponto
inputInts = [1, 2, 3]
inputStrings = ['Hello', 'world!']
inputBuffer = bytearray()
inputBuffer.append(78)
inputBuffer.append(42)
N = 1000
particles = []
serverIP = '127.0.0.1'
serverPort = 19999
leftMotorHandle = 0
vLeft = 0.
rightMotorHandle = 0
vRight = 0.
sensorHandle = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

l = 0.3310
noDetectionDist = 0.5
maxDetectionDist = 0.3
detect = [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]
braitenbergL = [-0.2, -0.4, -0.6, -0.8, -1, -1.2, -1.4, -1.6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
braitenbergR = [-1.6, -1.4, -1.2, -1, -0.8, -0.6, -0.4, -0.2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
# rad/s
v0 = 2
wheelR = 0.195 / 2.0
afterTeta = 0
beforeTeta = 0
turn = 0
result = []
vl = v0
vr = v0
myRobot = robot.robot()
myRobot.set_noise(0.0, 0.0, 0)
stepTime = 0.005

clientID = vrep.simxStart(serverIP, serverPort, True, True, 2000, 5)
vrep.simxSynchronous(clientID, True)
vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot_wait)

if clientID <> -1:

    print ('Servidor conectado!')
    util = util.util()


    # erro, objectHandles = vrep.simxGetObjects(clientID, vrep.sim_object_shape_type, vrep.simx_opmode_oneshot_wait)
    objectHandles = []
    erro, handle = vrep.simxGetObjectHandle(clientID, "ConcretBlock", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    for i in range(4):
        erro, handle = vrep.simxGetObjectHandle(clientID, "ConcretBlock#%d" % i, vrep.simx_opmode_oneshot_wait)
        objectHandles.append(handle)

    erro, handle = vrep.simxGetObjectHandle(clientID, "window140cm", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "window140cm0", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "sofa", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    # erro, handle = vrep.simxGetObjectHandle(clientID, "customizableTable#2", vrep.simx_opmode_oneshot_wait)
    # objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "sofa0", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "window140cm1", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "door", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "door#0", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "highTable", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    erro, handle = vrep.simxGetObjectHandle(clientID, "highTable0", vrep.simx_opmode_oneshot_wait)
    objectHandles.append(handle)
    segments = util.getSegments(clientID, objectHandles)


    erroPioneer, handlePioneer = vrep.simxGetObjectHandle(clientID, 'Pioneer_p3dx',
                                                          vrep.simx_opmode_oneshot_wait)

    erro, leftMotorHandle = vrep.simxGetObjectHandle(clientID, 'Pioneer_p3dx_leftMotor',
                                                     vrep.simx_opmode_oneshot_wait)
    if erro <> 0:
        print 'Handle do motor esquerdo nao encontrado!'
    else:
        print 'Conectado ao motor esquerdo!'

    erro, rightMotorHandle = vrep.simxGetObjectHandle(clientID, 'Pioneer_p3dx_rightMotor',
                                                      vrep.simx_opmode_oneshot_wait)
    if erro <> 0:
        print 'Handle do motor direito nao encontrado!'
    else:
        print 'Conectado ao motor direito!'

    for i in range(16):
        erro, sensorHandle[i] = vrep.simxGetObjectHandle(clientID, "Pioneer_p3dx_ultrasonicSensor%d" % (i + 1),
                                                         vrep.simx_opmode_oneshot_wait)
        if erro <> 0:
            print "Handle do sensor Pioneer_p3dx_ultrasonicSensor%d nao encontrado!" % (i + 1)
        else:
            print "Conectado ao sensor Pioneer_p3dx_ultrasonicSensor%d!" % (i + 1)
            erro, state, coord, detectedObjectHandle, detectedSurfaceNormalVector = vrep.simxReadProximitySensor(
                clientID,
                sensorHandle[i],
                vrep.simx_opmode_oneshot_wait)

    particles = util.particlesInit()
    util.drawParticles(particles, clientID)

    erroPioneerPose, pioneerPose = vrep.simxGetObjectPosition(clientID, handlePioneer, -1, vrep.simx_opmode_oneshot_wait)

    vrep.simxSetObjectOrientation(clientID, handlePioneer, -1, {0, 0, 0}, vrep.simx_opmode_oneshot_wait)

    # inicia o proximo passo da simulacao
    trigger = vrep.simxSynchronousTrigger(clientID)
    stepExecution = 0
    while vrep.simxGetConnectionId(clientID) != -1:
        stepExecution = stepExecution + 1
        erroPioneerPose, pioneerPose = vrep.simxGetObjectPosition(clientID, handlePioneer, -1, vrep.simx_opmode_oneshot_wait)
        pioneerX = pioneerPose[0]
        pioneerY = pioneerPose[1]
        for i in range(16):
            erro, state, coord, detectedObjectHandle, detectedSurfaceNormalVector = vrep.simxReadProximitySensor(
                clientID, sensorHandle[i], vrep.simx_opmode_oneshot_wait)
            if erro == 0:
                dist = coord[2]
                if state > 0 and dist < noDetectionDist:
                    if dist < maxDetectionDist:
                        dist = maxDetectionDist

                    detect[i] = 1 - ((dist - maxDetectionDist) / (noDetectionDist - maxDetectionDist))
                else:
                    detect[i] = 0
            else:
                detect[i] = 0
        vLeft = v0
        vRight = v0
        for i in range(16):
            vLeft = vLeft + braitenbergL[i] * detect[i]
            vRight = vRight + braitenbergR[i] * detect[i]

        erro = vrep.simxSetJointTargetVelocity(clientID, leftMotorHandle, vLeft, vrep.simx_opmode_oneshot_wait)
        erro = vrep.simxSetJointTargetVelocity(clientID, rightMotorHandle, vRight, vrep.simx_opmode_oneshot_wait)

        # Fim de um passo da simulacao
        trigger = vrep.simxSynchronousTrigger(clientID)
        endStepSim = vrep.simxGetPingTime(clientID)

        erroPioneerPose, pioneerPose = vrep.simxGetObjectPosition(clientID, handlePioneer, -1, vrep.simx_opmode_oneshot_wait)

        returnCode, eulerAngles = vrep.simxGetObjectOrientation(clientID, handlePioneer, -1, vrep.simx_opmode_oneshot_wait)

        angulo = eulerAngles[0]

        print ("angulo = ", angulo)

        if vRight == vLeft and vRight > 0:
            print ("------- vRight == vLeft ------ stepTime = ", stepTime)
            result = util.moveCase1(vLeft, vRight, pioneerX, pioneerY, beforeTeta, 0.05)
        else:
            print ("------- vRight != vLeft ------")
            result = util.moveCase2(vLeft, vRight, pioneerX, pioneerY, beforeTeta, stepTime)

        xResult, yResult = util.convertCoordToWorld100(result[0], result[1])
        xp, yp = util.convertCoordToWorld100(pioneerX, pioneerY)
        r = math.sqrt((xResult - xp) ** 2 + (yResult - yp) ** 2)
        afterTeta = result[2]
        turn = afterTeta - beforeTeta

        pontoBB = ponto.ponto(pioneerPose[0] + 10 * math.cos(afterTeta),
                              pioneerPose[1] + 10 * math.sin(afterTeta))
        inputFloats = [pioneerPose[0], pioneerPose[1], pontoBB.x, pontoBB.y]
        # vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
        #                             "desenharLinha2", inputInts, inputFloats, inputStrings,
        #                             inputBuffer, vrep.simx_opmode_oneshot_wait)
        detectedObjectHandleList = []
        coordList = []
        Z = []
        coordLandmarks = {}
        coordRectDetected = []
        angulosDeDeteccao = {}
        for i in range(16):
            erro, state, coord, detectedObjectHandle, detectedSurfaceNormalVector = vrep.simxReadProximitySensor(
                clientID, sensorHandle[i], vrep.simx_opmode_oneshot_wait)
            erroReadDist, readDist = vrep.simxReadDistance(clientID, detectedObjectHandle, vrep.simx_opmode_oneshot_wait)

            # pegar leitura dos sensores 1,2,5,7,8,13
            if i in (0, 1, 4, 6, 7, 12):
                if state:
                    print ("sensor ", i + 1)
                    handle = sensorHandle[i]
                    errorCoord, coordInOtherFrame = vrep.simxGetObjectPosition(clientID, detectedObjectHandle, -1, vrep.simx_opmode_oneshot_wait)
                    xcoord, ycoord = util.convertCoordToWorld100(coordInOtherFrame[0], coordInOtherFrame[1])
                    newLandmark = landmark.landmark(xcoord, ycoord, detectedObjectHandle)
                    coordLandmarks[i] = newLandmark
                    handleSensor = sensorHandle[i]
                    # returnCode, eulerAngles = vrep.simxGetObjectOrientation(clientID, handlePioneer, -1,
                    #                                                         vrep.simx_opmode_oneshot_wait)
                    angulo = afterTeta

                    erro, orientationInOtherFrame = vrep.simxGetObjectOrientation(clientID, handle, -1, vrep.simx_opmode_oneshot_wait)
                    alpha = 10
                    if i == 0:
                        alpha = -angulo - math.pi/2
                    elif i == 7:
                        alpha = -angulo + math.pi/2
                    elif i == 4:
                        alpha = -angulo
                    elif i == 1:
                        alpha = -angulo - math.pi/4
                    elif i == 6:
                        alpha = -angulo + math.pi/4
                    elif i == 12:
                        alpha = -angulo - math.pi

                    alpha *= -1
                    if alpha != -10:
                        angulosDeDeteccao[i] = alpha
                        # pontoBB = ponto.ponto(pioneerPose[0] + 10 * math.cos(alpha), pioneerPose[1] + 10 * math.sin(alpha))
                        # inputFloats = [pioneerPose[0], pioneerPose[1], pontoBB.x, pontoBB.y]
                        # vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
                        #                         "desenharLinha2", inputInts, inputFloats, inputStrings,
                        #                         inputBuffer, vrep.simx_opmode_oneshot_wait)

        xAtual, yAtual = util.convertCoordToWorld100(pioneerPose[0], pioneerPose[1])
        if len(angulosDeDeteccao) != 0:
            # Z = myRobot.sense(coordLandmarks, xAtual, yAtual)
            Z = myRobot.sense2(clientID, coordLandmarks, xAtual, yAtual, afterTeta, angulosDeDeteccao)

        # for i in len(angulosDeDeteccao):
        #     pontoBB = ponto.ponto(pioneerPose[0] + 10 * math.cos(alpha), pioneerPose[1] + 10 * math.sin(alpha))
        #     inputFloats = [pioneerPose[0], pioneerPose[1], pontoBB.x, pontoBB.y]
        #     vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
        #                         "desenharLinha2", inputInts, inputFloats, inputStrings,
        #                         inputBuffer, vrep.simx_opmode_oneshot_wait)
        particles = myRobot.updateParticleSet(turn, r, particles, afterTeta)
        # weight = myRobot.importantWeight(particles, coordLandmarks, Z)
        if len(angulosDeDeteccao) != 0:
            weight = myRobot.importantWeight(clientID, particles, angulosDeDeteccao, segments, Z)
            rp = robot.robot()
            rp.set(xAtual, yAtual, 0)
            particles = myRobot.resampling(particles, weight, rp)

        beforeTeta = afterTeta
        util.drawParticles(particles, clientID)
        endStepSim = vrep.simxGetPingTime(clientID)

    # Imprime as particulas
    util.printPaticles(particles)

    # fechando conexao com o servidor
    vrep.simxFinish(clientID)
    print 'Conexao fechada!'
else:
    print 'Problemas para conectar o servidor!'