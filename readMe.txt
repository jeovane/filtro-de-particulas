Tutorial

Para executar e testar o programa é necessário instalar o seguite:

1 - Baixar e instalar o simulador V-REP (link para download: http://www.coppeliarobotics.com/downloads.html)
2 - Instalar python versão 2.x.x ou 3.x.x (link para download: https://www.python.org/downloads/)

Após a instalação de v-rep e python, baixe o projeto. Em seguida inicie o v-rep da seguinte forma (Ambiente Linux):

1 - Extrair o conteúdo baixado (v-rep)
2 - Execute o arquivo vrep.sh

Na pasta cenário do projeto, estão os 10 cenários usados para testar o algoritmo implementado. Para reporduzir o experimento basta abrir o cenário desejado no v-rep e executar como explicado a seguir:

1 - No v-rep clique em File > Open scene. Em seguida escolhe o cenário desejado na pasta cenarios do projeto para abri-lo no v-rep.
2 - Após abrir o cenário desejado no v-rep, inicie a simulação clicando no botão de start situado na parte superior direito do v-rep.
3 - Após iniciar a simulação, execute o arquivo filtroDeParticulas.py
