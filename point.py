class point:
    def __init__(self, xMin, xMax, yMin, yMax, centerObjectPosition):
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax
        self.xCenterObjectPoint = centerObjectPosition[0]
        self.yCenterObjectPoint = centerObjectPosition[1]
