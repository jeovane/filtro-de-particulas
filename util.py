import json

import math
import random

import robot
import point
import vrep
import numpy as np
from intersection.ponto import ponto
from intersection.segment import segment

inputInts = [1, 2, 3]
inputStrings = ['Hello', 'world!']
inputBuffer = bytearray()
inputBuffer.append(78)
inputBuffer.append(42)

particleNumber = 1000
l = 0.3310
wheelR = 0.195 / 2.0


class util:
    # imprime as particulas num arquivo para serem analizadas

    def imprimeNoArquivo(self, particles, turn, r):
        file = open("particulas.txt", 'a')
        file.write("--------------------------antes x-------------------------------\n")
        file.write("turn = " + str(turn) + "\n")
        file.write("r = " + str(r) + "\n")

        for i in range(len(particles)):
            result = json.dumps([str(particles[i].x), str(particles[i].y), str(particles[i].orientation)])
            file.write(result)
            file.write("\n")
        file.write("--------------------------depois x---------------------------\n")

    # inicializa as particulas aleatoriamente
    def particlesInit(self):
        # fixedPositions = [[1.925, 1.525], [0.3 , 1.55], [-1.27, 1.52], [1.97, -1.57], [0.32, -1.97], [-1.3, -1.4]]
        #
        # for i in range(len(fixedPositions)):
        #     x, y = self.convertCoordToWorld100(fixedPositions[i][0], fixedPositions[i][1])
        #     fixedPositions[i][0] = x
        #     fixedPositions[i][1] = y
        # pos = -1

        particles = []

        for i in range(particleNumber):
            point = robot.robot()
            # pos = (pos + 1) % len(fixedPositions)
            # posNoise = random.random() * 10
            #
            # point.set(fixedPositions[pos][0] + posNoise, fixedPositions[pos][1] + posNoise, 0)
            point.set_noise(0.05, 0.05, 5.0)
            particles.append(point)
        return particles

    # envia as coordenadas das particulas para serem desenhadas no vrep
    def drawParticles(self, particles, clientID):
        inputFloats = []
        for i in range(len(particles)):
            inputFloats.append((particles[i].x / 20) - 2.5)
            inputFloats.append((particles[i].y / 20) - 2.5)

        showdebola = vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
                                                 "desenharAsParticulas", inputInts, inputFloats, inputStrings,
                                                 inputBuffer, vrep.simx_opmode_oneshot_wait)
    def desenharPontos(self, ponto, clientID):
        showdebola = vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
                                                 "desenharPontos", inputInts, ponto, inputStrings,
                                                 inputBuffer, vrep.simx_opmode_oneshot_wait)
#     convertFrame = function(intList, list, string, buffer)
#     matrix = simGetObjectMatrix(intList[1], intList[2])
#     return matrix, {}, {}, ''
#
#
# end
    def changeFrame(self, handle, frame, clientID):
        intarray = [handle, frame]
        resultado = vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
                                                    "convertFrame", intarray, [], [],
                                                inputBuffer, vrep.simx_opmode_oneshot_wait)
        return resultado


    def show(self, handle, clientID):
        intarray = [handle]
        i, v, n = vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
                                                    "show", intarray, [], [],
                                                inputBuffer, vrep.simx_opmode_oneshot_wait)
        return v

    # este eh o caso quando as velocidades aplicadas nas duas rodas forem iguais
    def moveCase1(self, vLeft, vRight, pioneerX, pioneerY, beforeTeta, stepTime):

        x = pioneerX + vLeft * wheelR * stepTime * math.cos(beforeTeta)
        y = pioneerY + vRight * wheelR * stepTime * math.sin(beforeTeta)
        res = [x, y, beforeTeta]
        return res

    # este eh o caso quando as velocidades aplicadas nas duas rodas forem diferentes
    def moveCase2(self, vLeft, vRight, pioneerX, pioneerY, beforeTeta, stepTime):
        w = (vRight - vLeft) / l

        if vRight == 0 or vLeft == 0:
            R = l / 2
        elif vLeft == -vRight:
            R = 0
        else:
            R = (l / 2) * ((vLeft + vRight) / (vRight - vLeft))

        iccx = pioneerX - R * math.sin(beforeTeta)
        iccy = pioneerY + R * math.cos(beforeTeta)

        matrix1 = np.array([(math.cos(w * stepTime), - math.sin(w * stepTime), 0),
                            (math.sin(w * stepTime), math.cos(w * stepTime), 0),
                            (0, 0, 1)])
        matrix2 = np.array([(pioneerX - iccx), (pioneerY - iccy), (beforeTeta)])
        matrix3 = np.array([(iccx), (iccy), (w * stepTime)])
        product = matrix1.dot(matrix2)
        result = product + matrix3
        return result

    # imprime as particulas
    def printPaticles(self, particles):
        for i in range(len(particles)):
            print particles[i]

    def convertCoordToWorld100(self, xv, yv):

        x = (xv + 2.5) * 20
        y = (yv + 2.5) * 20
        return x, y

    def getRectangle(self, clientID, detectedObjectHandle):
        erro, minX = vrep.simxGetObjectFloatParameter(clientID, detectedObjectHandle, 15, vrep.simx_opmode_oneshot_wait)
        erro, maxX = vrep.simxGetObjectFloatParameter(clientID, detectedObjectHandle, 18, vrep.simx_opmode_oneshot_wait)
        erro, minY = vrep.simxGetObjectFloatParameter(clientID, detectedObjectHandle, 16, vrep.simx_opmode_oneshot_wait)
        erro, maxY = vrep.simxGetObjectFloatParameter(clientID, detectedObjectHandle, 19, vrep.simx_opmode_oneshot_wait)
        erro, centerObjPosition = vrep.simxGetObjectPosition(clientID, detectedObjectHandle, -1, vrep.simx_opmode_oneshot_wait)

        boxMinX = centerObjPosition[0] + minX
        boxMaxX = centerObjPosition[0] + maxX
        boxMinY = centerObjPosition[1] + minY
        boxMaxY = centerObjPosition[1] + maxY
        coordRect = point.point(boxMinX, boxMaxX, boxMinY, boxMaxY, centerObjPosition)
        return coordRect

    def getSegments(self, clientID, objectHandles):

        segments = []
        for i in range(len(objectHandles)):
            boundingBox = self.getRectangle(clientID, objectHandles[i])
            x, y = self.convertCoordToWorld100(boundingBox.xMin, boundingBox.yMin)
            ponto1 = ponto(x, y)
            x, y = self.convertCoordToWorld100(boundingBox.xMin, boundingBox.yMax)
            ponto2 = ponto(x, y)
            newSegment = segment(ponto1, ponto2)
            segments.append(newSegment)

            inputFloats = [boundingBox.xMin, boundingBox.yMin, boundingBox.xMin, boundingBox.yMax]

            # vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
            #                             "desenharLinha", inputInts, inputFloats, inputStrings,
            #                             inputBuffer, vrep.simx_opmode_oneshot_wait)

            x, y = self.convertCoordToWorld100(boundingBox.xMin, boundingBox.yMax)
            ponto1 = ponto(x, y)
            x, y = self.convertCoordToWorld100(boundingBox.xMax, boundingBox.yMax)
            ponto2 = ponto(x, y)
            newSegment = segment(ponto1, ponto2)
            segments.append(newSegment)
            inputFloats = [boundingBox.xMin, boundingBox.yMax, boundingBox.xMax, boundingBox.yMax]

            # vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
            #                             "desenharLinha", inputInts, inputFloats, inputStrings,
            #                             inputBuffer, vrep.simx_opmode_oneshot_wait)

            x, y = self.convertCoordToWorld100(boundingBox.xMax, boundingBox.yMin)
            ponto1 = ponto(x, y)
            x, y = self.convertCoordToWorld100(boundingBox.xMax, boundingBox.yMax)
            ponto2 = ponto(x, y)
            newSegment = segment(ponto1, ponto2)
            segments.append(newSegment)
            inputFloats = [boundingBox.xMax, boundingBox.yMin, boundingBox.xMax, boundingBox.yMax]

            # vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
            #                             "desenharLinha", inputInts, inputFloats, inputStrings,
            #                             inputBuffer, vrep.simx_opmode_oneshot_wait)


            x, y = self.convertCoordToWorld100(boundingBox.xMin, boundingBox.yMin)
            ponto1 = ponto(x, y)
            x, y = self.convertCoordToWorld100(boundingBox.xMax, boundingBox.yMin)
            ponto2 = ponto(x, y)
            newSegment = segment(ponto1, ponto2)
            segments.append(newSegment)
            inputFloats = [boundingBox.xMin, boundingBox.yMin, boundingBox.xMax, boundingBox.yMin]

            # vrep.simxCallScriptFunction(clientID, "Pioneer_p3dx", vrep.sim_scripttype_childscript,
            #                             "desenharLinha", inputInts, inputFloats, inputStrings,
            #                             inputBuffer, vrep.simx_opmode_oneshot_wait)
        return segments